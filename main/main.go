package main

import (
	"fmt"

	"bitbucket.org/ValentinLethiot/userEvent/domain"
	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
)

var lesEvents = []event.UserRegisteredEvent{
	event.UserRegisteredEvent{
		ID:        1,
		Firstname: "Val",
		Lastname:  "Let",
		Age:       21,
		Email:     "a@a.a",
		Password:  "azerty123",
	},
	event.UserRegisteredEvent{
		ID:        1,
		Firstname: "Valentin",
		Lastname:  "Let",
		Age:       23,
		Email:     "a@a.a",
		Password:  "azerty123",
	},
}

func main() {
	user, err := domain.NewUser(lesEvents)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(user)
	}
}
