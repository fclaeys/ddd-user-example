package domain

import (
)

func UserRegisterDecision(command RegisterUserCommand) *UserRegisteredEvent {
	return NewUserRegisteredEvent(
		command.ID,
		command.Age,
		command.Firstname,
		command.Lastname,
		command.Email,
		command.Password,
	)
}

func UpdatePasswordDecision(command UpdatePasswordCommand) *PasswordUpdatedEvent {
	if len(command.NewPassword) < 8 {
		return NewPasswordUpdatedEvent(command.ID,command.OldPassword)
	}
	if command.NewPassword == command.OldPassword {
		return NewPasswordUpdatedEvent(command.ID,command.OldPassword)
	}
	return NewPasswordUpdatedEvent(command.ID,command.NewPassword)
}
