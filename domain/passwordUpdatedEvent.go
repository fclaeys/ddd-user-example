package domain

type PasswordUpdatedEvent struct {
	ID       int
	Password string
}

func NewPasswordUpdatedEvent(ID int, pass string) *PasswordUpdatedEvent {
	return &PasswordUpdatedEvent{
		ID:       ID,
		Password: pass,
	}
}
