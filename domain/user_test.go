package domain

import (
	"fmt"
	"github.com/matryer/is"
	"reflect"
	"testing"
)

func TestRegisterUser_returnsUserRegistred(t *testing.T) {
	//Given
	is := is.New(t)
	command := NewRegisterUserCommand(1,42,"jon", "doe", "jon.doe@gmail.com", "yack1234")
	user := NewUser()

	//When
	event, err := user.Register(command)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(event), reflect.TypeOf((*UserRegisteredEvent)(nil)))
	is.Equal(1, event.ID)
	is.Equal(42, event.Age)
	is.Equal("jon", event.Firstname)
	is.Equal("doe", event.Lastname)
	is.Equal("jon.doe@gmail.com", event.Email)
	is.Equal("yack1234", event.Password)
}

func TestUpdatePassword_returnPasswordUpdated(t *testing.T) {
	//Given
	is := is.New(t)
	ure := NewUserRegisteredEvent(1,42,"jon", "doe", "jon.doe@gmail.com", "yack1234")
	pce := NewPasswordUpdatedEvent(1, "yack+1234")
	user, _ := NewUserFromEvents([]Event{*ure, *pce})

	command := NewUpdatePasswordCommand(1, "yack+1234", "yack1234")

	//When
	event, err := user.ChangePassword(command)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(event), reflect.TypeOf((*PasswordUpdatedEvent)(nil)))
	is.Equal(1, event.ID)
	is.Equal("yack1234", event.Password)
}

func TestUpdatePassword_returnErrorWhenSamePassword(t *testing.T) {
	//Given
	is := is.New(t)
	ure := NewUserRegisteredEvent(1,42,"jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]Event{*ure})

	command := NewUpdatePasswordCommand(1, "yack1234", "yack1234")

	//When
	event, err := user.ChangePassword(command)

	//Then
	is.Equal(fmt.Errorf("Invalid password"), err)
	is.Equal(nil, event)
}

