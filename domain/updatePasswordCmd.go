package domain

type UpdatePasswordCommand struct {
	ID          int
	OldPassword string
	NewPassword string
}

func NewUpdatePasswordCommand(ID int, oldPass, newPass string) UpdatePasswordCommand {
	return UpdatePasswordCommand{
		ID:          ID,
		OldPassword: oldPass,
		NewPassword: newPass,
	}
}
