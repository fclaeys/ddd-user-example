package domain

import (
	"fmt"
)

type decisionProjection struct {
	ID        int
	Firstname string
	Lastname  string
	Age       int
	Email     string
	Password  string
}

func (dp *decisionProjection) applyUserRegistredEvent(e UserRegisteredEvent) {
	dp.ID = e.ID
	dp.Age = e.Age
	dp.Password = e.Password
	dp.Firstname = e.Firstname
	dp.Lastname = e.Lastname
	dp.Email = e.Email
}

func (dp *decisionProjection) applyPasswordUpdatedEvent(e PasswordUpdatedEvent) {
	dp.Password = e.Password
}

type UserAggregate struct {
	*decisionProjection
}

func (aggregate UserAggregate) Register(command RegisterUserCommand) (*UserRegisteredEvent, error) {
	aggregate.ID = command.ID
	aggregate.Lastname = command.Lastname
	aggregate.Firstname = command.Firstname
	aggregate.Age = command.Age
	aggregate.Email = command.Email
	aggregate.Password = command.Password

	return NewUserRegisteredEvent(
		command.ID,
		command.Age,
		command.Firstname,
		command.Lastname,
		command.Email,
		command.Password), nil
}

func (aggregate UserAggregate) ChangePassword(command UpdatePasswordCommand) (*PasswordUpdatedEvent, error) {
	if aggregate.Password == command.NewPassword {
		return nil, fmt.Errorf("Invalid password")
	}

	aggregate.Password = command.NewPassword

	return NewPasswordUpdatedEvent(
		command.ID,
		command.NewPassword), nil
}

func NewUser() *UserAggregate{
		return &UserAggregate{
			decisionProjection: &decisionProjection{},
		}
}

//seulement des UserRegisteredEvent pour l'instant
func NewUserFromEvents(events []Event) (*UserAggregate, error) {
	if len(events) == 0 {
		return nil, fmt.Errorf("No events")
	}

	us := NewUser()
	for _, ev := range events {
		switch ev.(type) {
		case UserRegisteredEvent:
			ure, _ := ev.(UserRegisteredEvent)
			us.applyUserRegistredEvent(ure)
		case PasswordUpdatedEvent:
			pce, _ := ev.(PasswordUpdatedEvent)
			us.applyPasswordUpdatedEvent(pce)
		}


	}

	return us, nil
}
