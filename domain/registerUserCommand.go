package domain

type RegisterUserCommand struct {
	ID        int
	Firstname string
	Lastname  string
	Age       int
	Email     string
	Password  string
}

func NewRegisterUserCommand(id, age int, fn, ln, mail, pass string) RegisterUserCommand {
	return RegisterUserCommand{
		ID:        id,
		Firstname: fn,
		Lastname:  ln,
		Age:       age,
		Email:     mail,
		Password:  pass,
	}
}
